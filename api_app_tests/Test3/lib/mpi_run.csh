#!/bin/bash
EXE=$HOME/dftbplus-18.2/build/call_lib.exe       
OUT=terminal.out
export OMPI_MCA_hwloc_base_binding_policy=none
export OMP_NUM_THREADS=1
mpirun -np 1 --bind-to none $EXE #> $OUT
