#!/bin/bash

rm band.out \
   charges.bin \
   detailed.out \
   dftb_pin.hsd \
   geo_end.gen \
   geo_end.xyz \
   pdos.C.1.out \
   pdos.C.2.out \
   terminal.out \
   forces_* 
