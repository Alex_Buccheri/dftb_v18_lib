#!/bin/bash
#EXE=$HOME/dftbplus-18.2/_build/prog/dftb+/dftb+  # Exe with legacy make
EXE=$HOME/dftbplus-18.2/build/dftb+.exe          # Exe with my cmake
#EXE=$HOME/dftbplus-18.2/build/call_lib.exe        # Test exe linked to DFTB lib - both built with my cmake 

OUT=terminal.out
export OMPI_MCA_hwloc_base_binding_policy=none
export OMP_NUM_THREADS=1
mpirun -np 1 --bind-to none $EXE #> $OUT
