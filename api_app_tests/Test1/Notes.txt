-------------------------------------------------------
Compare forces from running exe to forces from running as a library.
-------------------------------------------------------
Difference in numbers is quite surprising - assume it's from atomic positions in
CONFIG vs in graphene.gen and different working precision 
kdiff3 exe/results.tag lib/results.tag

-------------------------------------------------------
Compare forces output from DFTB (running as lib) vs forces returned
from caller - Checks that the correct thing is being returned 
-------------------------------------------------------
cd lib
kdiff3 results.tag forces.dat
Note:
	Delete headers in both files. 
	results.tag contains additional data after forces => ignore 
