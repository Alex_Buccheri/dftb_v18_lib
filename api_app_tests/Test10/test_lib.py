#!/usr/bin/env python3
#------------------------------------------------
# Compare total energy and forces for N MD steps
# between DFTB+ exe and DFTB lib 
#------------------------------------------------

import os
import sys
import pathlib 
import subprocess
import numpy as np

def mkdir(dir_name):
    dir_ = pathlib.Path(dir_name)
    if(dir_.exists()):
        print("Directory already exists: ",dir_name)
    else:
       os.system("mkdir "+dir_name)
       print("Created directory: ",dir_name)
    return 

def delete_dir(dir_name):
    dir_ = pathlib.Path(dir_name)
    if(dir_.exists()):
        os.system("rm -r "+dir_name)
        print("Deleted directory: ",dir_name)
    return

#Return total energy from DFTB output, in eV
def extract_Etotal(term_out):
    TotalEnergyStr = str( subprocess.check_output("grep 'Total Energy' "+term_out, shell=True) )
    temp = TotalEnergyStr.split()
    Etotal=[]
    for i in range(0,len(temp)):
        if('eV' in temp[i]): Etotal.append(float(temp[i-1]))
    return Etotal 


def add_string_to_file(string,fname):
    with open(fname, 'a+') as file:
        file.write(string)
    return

def mpi_run(MPI_np,EXE,output):
   #Same line as a new shell is initialised on each os.system call
    os.system("export OMP_NUM_THREADS=1 && mpirun -np "+str(MPI_np)+" "+EXE+" > "+output)
    return 

#Hard-coded to 3 columns per lin 
def readNlines(fname,Nlines,skiprows):
    fid = open(fname, "r")
    #Read line by line - Don't save header lines
    for i in range(0,skiprows):
        line = fid.readline()
    force = np.zeros(shape=(3,Nlines))
    tmp =[]
    for i in range(skiprows,Nlines+skiprows):
        j=i-(skiprows)
        tmp = fid.readline().split()[0:3]
        force[:,j] = [float(tmp[0]),float(tmp[1]),float(tmp[2])]
    fid.close()
    return force
    

#----------------------
# Settings
#----------------------
#Run lib, exe, force (comparison) 
run_type='force'

#Number of MPI processes
MPI_np=2

#MD steps 
initial_step = 4
final_step = 5

#Paths
#DFTB_root='/Users/alexanderbuccheri/dftbplus-18.2/api_app_tests/Test8'
DFTB_root='/home/alex/dftbplus-18.2/api_app_tests/Test10'
DFTB_lib=DFTB_root+'/build/call_lib_test10.exe'
DFTB_exe=DFTB_root+'/build/dftb+.exe' 

#Check executables exist
if( run_type != 'force'):
    executable = pathlib.Path(DFTB_lib)
    if not executable.exists():
        print('Executable that calls DFTB lib does not exist')
        print('Check path in script and ensure call_lib.exe has been built')
        sys.exit('Script stops')
    executable = pathlib.Path(DFTB_exe)
    if not executable.exists():
        print('DFTB standalone executable does not exist')
        print('Check path in script and ensure dftb+.exe has been built')
        sys.exit('Script stops')    

    
print('NOTE: Must do make install -j to ensure using correct executables in calculations')

#----------------------
#Library calculations
#----------------------
lib_dir="lib"
Etotal_lib = []

if(run_type=='lib'):
    print('Running DFTB+ lib via call_lib.f90 with '+str(final_step)+' steps on '+\
          str(MPI_np)+' processes.')
    print('Note, N MD steps in call_lib.f90 must be consistent with this setting')
    #Must generate .gen files (check they're available)
    delete_dir(lib_dir)
    mkdir(lib_dir)
    os.system("cp inputs/CONFIG inputs/dftb_in.hsd lib/.")
    os.chdir(lib_dir)
    mpi_run(MPI_np,DFTB_lib,"term.out")
    Etotal_lib = extract_Etotal("term.out")
    os.chdir('../')

#------------------------
#Standalone calculations
#------------------------
fname = 'dftb_in.hsd'

dftb_in_str = \
'''Hamiltonian = DFTB {
  SCC = Yes
  #SCCTolerance = 5e-1
  MaxAngularMomentum = {
    C = "p"
    H = "s"
  }
  Filling = Fermi {
    Temperature [Kelvin] = 100
  }
  SlaterKosterFiles = Type2FileNames {
    Prefix = "../../../parameters/"
    Separator = "-"
    Suffix = ".skf"
  }
}

Options = {
   WriteResultsTag=Yes
   WriteDetailedOut=No
}

Analysis = {
  CalculateForces = Yes
}

ParserOptions {
  ParserVersion = 6
}'''

Etotal_exe=[]
exe_dir="exe"
 
if(run_type=='exe'):
    
    delete_dir(exe_dir)
    mkdir(exe_dir)

    #Set up N directories for calculations and execute in each
    for i in range(initial_step,final_step+1):
        exe_i_dir = exe_dir+'/'+str(i)
        mkdir(exe_i_dir)
        #os.system('cp lib/structure_'+str(i)+'.gen '+exe_i_dir+'/.')
        os.system('cp inputs/structure_'+str(i)+'.gen '+exe_i_dir+'/.')
        #Change to exe dir and create input 
        os.chdir(exe_i_dir)
        geo_str= \
        'Geometry = GenFormat {' +'\n'+ \
        ' <<< structure_'+str(i)+'.gen'    +'\n'+ \
        '}'
        add_string_to_file(geo_str,fname)
        add_string_to_file(dftb_in_str,fname)
        #Execute and extract total energies
        mpi_run(MPI_np,DFTB_exe,"term.out")
        Etotal_exe.append( extract_Etotal("term.out")[0] )
        os.chdir('../../')
                    
    
#---------------------------------
#Compare total energies and forces
#---------------------------------
if(run_type=='force'):
    #---------------------------------
    #Compare total energies (eV)
    #---------------------------------
    lib_dir="lib"
    exe_dir="exe"

    
    #Requires lib_dir and lib_dir/term.out to exist
    os.chdir(lib_dir)
    Etotal_lib = extract_Etotal("term.out")
    os.chdir('../')
    
    #Requires exe/0->N and term.out files to exist 
    for i in range(initial_step,final_step+1):
        exe_i_dir = exe_dir+'/'+str(i)
        os.chdir(exe_i_dir)
        Etotal_exe.append( extract_Etotal("term.out")[0] )
        os.chdir('../../')

    for i in range(0,final_step-initial_step+1):
        print('Abs diff in total energy (eV) for MD step ',i,': ',abs(Etotal_lib[i]-Etotal_exe[i]))

    #---------------------------------
    #Compare Forces    
    #---------------------------------
    Natoms = 568
    force_lib=np.zeros(shape=(3,Natoms))
    force_exe=np.zeros(shape=(3,Natoms))
    diff =np.zeros(shape=(3,Natoms))
    
    for i in range(initial_step,final_step+1):
        force_lib = np.loadtxt(lib_dir+'/forces_'+str(i)+'.dat',\
                               skiprows=1, unpack=True)
        frc_exe_fname = exe_dir+'/'+str(i)+'/results.tag'
        force_exe = readNlines(frc_exe_fname, Natoms, 5)
        diff[0:3,:] = force_lib[0:3,:]-force_exe[0:3,:]
    
    #print('diff')
    #for i in range(0,Natoms):
    #    print(i, diff[:,i])  

        avg_diff=np.mean(np.abs(diff))
        min_diff=np.min(np.abs(diff))
        max_diff=np.max(np.abs(diff))
        print('Abs Difference in forces (eV). min, avg, max:',min_diff,avg_diff,max_diff)
