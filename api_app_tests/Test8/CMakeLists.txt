#------------------------------------------------
# CMake for DFTB+ V18.2 
# Alexander Buccheri UoB 2018 
#------------------------------------------------

cmake_minimum_required(VERSION 3.02)
if(COMMAND cmake_policy)
  cmake_policy(SET CMP0048 NEW)
endif(COMMAND cmake_policy)

project(dftbplus  VERSION 18.2  LANGUAGES Fortran C)

#Includes
set( DFTB_ROOT "../../")

include(GNUInstallDirs)
include_directories(${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include)
include(${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/CMakeLists.txt)

#Cmake output subdirectories 
set( CMAKE_Fortran_MODULE_DIRECTORY ${CMAKE_BINARY_DIR}/modules)
set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/preprocessed)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/${DFTB_ROOT}/cmake/modules")

#Preprocessor for DFTB source  
set(FPP ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/external/fypp/bin/fypp)

#Default compiler flags, noting that they must be defined without quotation marks 
set(DFTB_FLAGS -DDEBUG=0 -DRELEASE=\"'release 18.2'\" -DWITH_SOCKETS -DWITH_MPI -DWITH_SCALAPACK)

#GCC or MKL
set(LIB_CHOICE "MKL")

#Compile DFTB+ as a library 
set(AS_LIB "true")

#Compiling without ARPACK and D3
set(WITH_ARPACK "false")
set(WITH_D3 "false")

#-----------------------------------------------------
# XMLf90 lib
#-----------------------------------------------------
set(XMLSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/external/xmlf90)
add_library(libxmlf90 SHARED
     ${XMLSRC}/flib_dom.f90
     ${XMLSRC}/m_dom_nodelist.f90
     ${XMLSRC}/m_dom_types.f90
     ${XMLSRC}/m_strings.f90
     ${XMLSRC}/m_dom_utils.f90
     ${XMLSRC}/m_dom_document.f90
     ${XMLSRC}/m_dom_namednodemap.f90
     ${XMLSRC}/m_dom_node.f90
     ${XMLSRC}/m_dom_error.f90
     ${XMLSRC}/m_dom_debug.f90
     ${XMLSRC}/flib_wxml.f90
     ${XMLSRC}/m_wxml_core.f90
     ${XMLSRC}/m_wxml_elstack.f90
     ${XMLSRC}/m_wxml_buffer.f90
     ${XMLSRC}/m_wxml_dictionary.f90
     ${XMLSRC}/m_wxml_text.f90
     ${XMLSRC}/m_dom_element.f90
     ${XMLSRC}/m_dom_attribute.f90
     ${XMLSRC}/m_dom_parse.f90
     ${XMLSRC}/flib_sax.f90
     ${XMLSRC}/m_xml_parser.f90
     ${XMLSRC}/m_elstack.f90
     ${XMLSRC}/m_buffer.f90
     ${XMLSRC}/m_fsm.f90
     ${XMLSRC}/m_entities.f90
     ${XMLSRC}/m_dictionary.f90
     ${XMLSRC}/m_charset.f90
     ${XMLSRC}/m_debug.f90
     ${XMLSRC}/m_reader.f90
     ${XMLSRC}/m_io.f90
     ${XMLSRC}/m_xml_error.f90
     ${XMLSRC}/m_converters.f90
      )

# fsockets lib
set(FSOCKETSSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/external/fsockets)
add_library(extfsockets SHARED ${FSOCKETSSRC}/sockets.c ${FSOCKETSSRC}/fsockets.f90)


#------------------------------------------------
# mpifx Lib 
#------------------------------------------------
set(MPIFXSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/external/mpifx/origin/src)

include_directories(
    ${MPIFXSRC}
    ${PROJECT_BINARY_DIR}/preprocessed/mpifx
    )

file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/preprocessed/mpifx)

set( MPIFX_SOURCE
  libmpifx
  mpifx_abort
  mpifx_allgather
  mpifx_allgatherv
  mpifx_allreduce
  mpifx_barrier
  mpifx_bcast
  mpifx_comm
  mpifx_common
  mpifx_constants
  mpifx_finalize
  mpifx_gather
  mpifx_gatherv
  mpifx_get_processor_name
  mpifx_helper
  mpifx_init
  mpifx_recv
  mpifx_reduce
  mpifx_scatter
  mpifx_send
  )

#Preprocess
foreach(file ${MPIFX_SOURCE})
      set(file_dependency ${MPIFXSRC}/${file}.F90)

      add_custom_command(
        OUTPUT ${PROJECT_BINARY_DIR}/preprocessed/mpifx/${file}.f90 
        COMMAND m4 -I${MPIFXSRC} ${MPIFXSRC}/${file}.F90 > ${PROJECT_BINARY_DIR}/preprocessed/mpifx/${file}.f90
        DEPENDS ${file_dependency}
        )

     set( MPIFX_SRC ${MPIFX_SRC} ${PROJECT_BINARY_DIR}/preprocessed/mpifx/${file}.f90)
endforeach(file)

add_library(libmpifx SHARED ${MPIFX_SRC} )


#------------------------------------------------
# Scalapack FX Lib 
#------------------------------------------------
set(SCALFXSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/external/scalapackfx/origin/src)

include_directories(
    ${SCALFXSRC}
    ${PROJECT_BINARY_DIR}/preprocessed/scalfx
    )

if(${LIB_CHOICE} STREQUAL "GCC")
   find_package(SCALAPACK REQUIRED)
   set(LIBRARIES ${LIBRARIES} ${SCALAPACK_LIBRARIES})
endif()


if(${LIB_CHOICE} STREQUAL "MKL")
   #MKL MPI and Scalapack (blas contained in scalapack)
   find_package(MKL 15.0.0 COMPONENTS ScaLAPACK)

   if(MKL_FOUND)
      include_directories(${MKL_INCLUDE_DIRS})
      link_directories(${MKL_LIBRARY_DIRS}) 
      set(LIBRARIES ${LIBRARIES} ${MKL_LIBRARIES})
   endif()
endif()


set(SCALFX_SOURCE 
          blacsfx
	  libscalapackfx
	  pblasfx
	  linecomm
	  scalapackfx_common
	  blacsgrid
	  scalapack
	  blacs
	  pblas
	  scalapackfx_tools
	  scalapackfx )

file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/preprocessed/scalfx)

#Preprocess
set(file " ")
foreach(file ${SCALFX_SOURCE})
      set(file_dependency ${SCALFXSRC}/${file}.F90)

      add_custom_command(
        OUTPUT ${PROJECT_BINARY_DIR}/preprocessed/scalfx/${file}.f90 
        COMMAND m4 -I${SCALFXSRC} ${SCALFXSRC}/${file}.F90 > ${PROJECT_BINARY_DIR}/preprocessed/scalfx/${file}.f90
        DEPENDS ${file_dependency}
        )

     set( SCALFX_SRC ${SCALFX_SRC} ${PROJECT_BINARY_DIR}/preprocessed/scalfx/${file}.f90)
endforeach(file)

add_library(libscalapackfx SHARED ${SCALFX_SRC})
target_link_libraries(libscalapackfx ${LIBRARIES})




#------------------------------------------------
# DFTB+ source code subdirectories
#------------------------------------------------
set(CSRC     ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_common)
set(DESRC   ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_derivs)
set(DFSRC   ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_dftb)
set(EXSRC    ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_extlibs)
set(GEOSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_geoopt)
set(ISRC      ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_io)
set(MASRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_math)
set(MDSRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_md)
set(MISRC   ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_mixer)
set(TISRC    ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_timedep)
set(TYSRC   ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_type)
set(PRSRC   ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/prg_dftb)

#----------------------------------------------
# Remove linear response code dependent on ARPACK
#----------------------------------------------
if(${WITH_ARPACK} STREQUAL "false")
     set(TISRC_FILES linresp)
endif()

#----------------------------------------------
# Remove DFTBD3 dependent on dftb-d3 
#----------------------------------------------
if(${WITH_D3} STREQUAL "false")
     #Must be list, not string 
     set(SOURCE_NAMES   EXSRC_FILES)
     set(RM_EXSRC_FILES  dftd3)

     foreach(source ${SOURCE_NAMES})
        foreach(remove  ${RM_EXSRC_FILES})
		   list(REMOVE_ITEM ${source} ${remove} )
	endforeach(remove)
     endforeach(source)
endif()





if(${AS_LIB} STREQUAL "true")
   #------------------------------------------------
   # DFTB+ source code specific to library 
   #------------------------------------------------
   set(API_PRSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/api/prg_dftb)
   set(API_PRSRC_FILES
         dftbplus_lib
         parser
	 main
	 globalenv
	)
   #-------------------------------------------------------
   # Remove source code from prog/dftb+/CMakelists.txt that has been
   # modified for library compilation of DFTB+
   #-------------------------------------------------------
     set( SOURCE_NAMES
     	  CSRC_FILES
     	  DESRC_FILES
     	  DFSRC_FILES
     	  EXSRC_FILES
     	  GEOSRC_FILES
     	  ISRC_FILES
     	  MASRC_FILES
     	  MDSRC_FILES
     	  MISRC_FILES
     	  TISRC_FILES
     	  TYSRC_FILES
     	  PRSRC_FILES)

	  foreach(source_var ${SOURCE_NAMES})
	     #In the case of dftbplus, the library interface name differs, hence specify in list explicitly
       	     foreach(remove_var   ${API_PRSRC_FILES} dftbplus)
		   list(REMOVE_ITEM ${source_var} ${remove_var} )
	     endforeach(remove_var)
	  endforeach(source_var)
endif()


#----------------------------------------------
#Preprocess DFTB+ source 
#----------------------------------------------
#Tuple structure: Source subdirectory\;List of source code in subdirectory
set(tuples
  "CSRC\;CSRC_FILES"
  "DESRC\;DESRC_FILES"
  "DFSRC\;DFSRC_FILES"
  "EXSRC\;EXSRC_FILES"
  "GEOSRC\;GEOSRC_FILES"
  "ISRC\;ISRC_FILES"
  "MASRC\;MASRC_FILES"
  "MDSRC\;MDSRC_FILES"
  "MISRC\;MISRC_FILES"
  "TISRC\;TISRC_FILES"
  "TYSRC\;TYSRC_FILES"
  "PRSRC\;PRSRC_FILES"
  "API_PRSRC\;API_PRSRC_FILES"
)

file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/preprocessed/dftb+)
set(SOURCE_CODE)

foreach(element ${tuples})
       #Get elements of the list
       list (GET element 0 dir)
       list (GET element 1 files)
       
       foreach(file ${${files}})
       	   set(file_dependency ${${dir}}/${file}.F90)

     	   add_custom_command(
		OUTPUT ${PROJECT_BINARY_DIR}/preprocessed/dftb+/${file}.f90 
        	COMMAND ${FPP} ${DFTB_FLAGS} -I${${dir}}
		-I${CMAKE_Fortran_MODULE_DIRECTORY} -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${${dir}}/${file}.F90 > ${PROJECT_BINARY_DIR}/preprocessed/dftb+/${file}.f90
        	DEPENDS ${file_dependency}
        	)

     	  set( SOURCE_CODE ${SOURCE_CODE} ${PROJECT_BINARY_DIR}/preprocessed/dftb+/${file}.f90)
       endforeach(file)
endforeach(element)


#-----------------------------------
# Compile DFTB+ 
#-----------------------------------
#find_package(LAPACK REQUIRED)

# Executable
if(${AS_LIB} STREQUAL "false")
     add_executable(dftb+.exe ${SOURCE_CODE})
     target_link_libraries(dftb+.exe
  	     libxmlf90
	     extfsockets
	     libmpifx
	     libscalapackfx
	     ${LAPACK_LIBRARIES})
endif()

# Library
if(${AS_LIB} STREQUAL "true")
     add_library(dftb SHARED ${SOURCE_CODE} )
     add_dependencies(dftb
             ${SOURCE_LIST}
	     libxmlf90
	     extfsockets
	     libmpifx
	     libscalapackfx)
     target_link_libraries(dftb
  	     libxmlf90
	     extfsockets
	     libmpifx
	     libscalapackfx
	     ${LAPACK_LIBRARIES})

   # Test library interface
    #set(DL_DIR "/Users/alexanderbuccheri/Codes/development/dl-poly-4.10")
    set(DL_DIR "/home/alex/dl-poly-4.10")

     #DLPOLY compiled modules - can't do this. Causes conflicts with the DFTB+ build 
     #include_directories(${DL_DIR}/build-dftb-gcc-serial/source)
     #All DLPOLY module dependencies therefore explicitly added
     
     add_executable(call_lib_test8.exe
                              ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/api/dftb_api.F90
			      ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/api/dftb_utils.F90
			      ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/api/dlpoly_utils.F90
			      ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/api/lib_callers/call_lib_test8.f90
			      ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_type/typegeometry.F90
			      ${DL_DIR}/source/configuration.F90
			      ${DL_DIR}/source/kinds.F90
			      ${DL_DIR}/source/comms.F90
			      ${DL_DIR}/source/particle.F90
			      ${DL_DIR}/source/site.F90
			      ${DL_DIR}/source/errors_warnings.F90
			      ${DL_DIR}/source/constants.F90
			      ${DL_DIR}/source/parse.F90
			      ${DL_DIR}/source/domains.F90
			      ${DL_DIR}/source/development.F90
			      ${DL_DIR}/source/netcdf.F90
			      ${DL_DIR}/source/io.F90
			      ${DL_DIR}/source/numerics.F90
			      ${DL_DIR}/source/thermostat.F90
			      ${DL_DIR}/source/electrostatic.F90
			      ${DL_DIR}/source/filename.F90
			      ${DL_DIR}/source/flow.F90
			      )    
     target_link_libraries(
             call_lib_test8.exe
             dftb
  	     libxmlf90
	     extfsockets
	     libmpifx
	     libscalapackfx
	     ${LAPACK_LIBRARIES})

     #Install targets
     install(TARGETS dftb libxmlf90 extfsockets libmpifx libscalapackfx
  	       RUNTIME DESTINATION bin
	       LIBRARY DESTINATION lib
	       ARCHIVE DESTINATION lib/static)
     install(DIRECTORY ${CMAKE_Fortran_MODULE_DIRECTORY}/ DESTINATION include/)

endif()




