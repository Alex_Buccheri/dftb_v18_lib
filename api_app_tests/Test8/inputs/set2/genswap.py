#!/usr/bin/env python3

# --------------------------------------------------------
# Swap data entries in .gen file produced with 2 processes
# so it can be compared to .gen produced with 1 process
# --------------------------------------------------------
#Libraries
import sys
import numpy as np
import os
import subprocess
import shutil


def strlist_to_floatlist(strlist):
     return [float(i) for i in strlist.split()]


fname = 'structure_0'

data = np.genfromtxt(fname+'.gen', dtype=str,  delimiter='\n')
natoms = 568

line1 = data[0]
line2 = data[1]
data = data[2:]
new_data = np.zeros(shape=(5,natoms))

#Move hydrogen blocks into correct positions
offset_j = [0,   5,   10,  15 ] 
offset_k = [481, 487, 534, 540]

for io in range(0,4):
    for i in range(0,5):
        j = i + offset_j[io]
        k = i + offset_k[io]
        new_data[:,k] = strlist_to_floatlist(data[j])
        #print(k, new_data[:,k])

#Add rest of data in, avoiding overwriting hydrogen elements
offset_old = 20

inew=0
for i in range(offset_old,natoms):
    new_data[:,inew] = strlist_to_floatlist(data[i])
    inew += 1
    if( inew in offset_k):
        inew += 5        


#Swap element index 1 and 2 around
index_swap={1:2,2:1}
for i in range(0,natoms):
    index = new_data[1,i]
    new_data[1,i] = index_swap[index]
    
#Output file      
fid=open(fname+'_convert.gen', mode='w')  #encoding='utf-8'
print('#Reordered gen file',file=fid)
print(' '+str(line1),file=fid)
line2 = 'C H '
print(line2,file=fid)
for i in range(0,natoms):
    print ('{0:3d} {1:2d} {2:10f} {3:10f} {4:10f}'.format(i+1,int(new_data[1,i]),*new_data[2:,i]),file=fid)
fid.close()
    

