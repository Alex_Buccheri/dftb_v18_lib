//Ref: https://github.com/UCL/GreatCMakeCookOff/blob/master/modules/arch/test_is_64bit.c
int main() {
#if __x86_64__
  return 1;
#else
  return 0;
#endif
}
