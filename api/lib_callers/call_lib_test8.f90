!------------------------------------------------------
! Test DLPOLY(v4.10)-DFTB+(v18.2) API 
!------------------------------------------------------
program call_lib
  !MPI
  use mpi,           only: mpi_init_thread,MPI_COMM_WORLD,MPI_THREAD_FUNNELED, &
                           mpi_comm_rank,mpi_comm_size
  !DFTB+
  use mpifx,         only: mpifx_comm
  use typegeometry,  only: TGeometry
  use dftbplus_lib,  only: dftbplus,MDstatus_type
  !DLPOLY
  use kinds,         only: wp
  use comms,         only: comms_type
  use configuration, only: configuration_type
  !API
  use dlpoly_utils,  only: read_DLPOLY_vars,  fill_DLPOLY_comm, move_atomic_positions,&
                           DLPOLY_initial_MDstep
  use dftb_api,      only: fill_DFTB_geometry,fill_DFTB_mpifx_comm
  use dftb_utils,    only: print_geometry_data,output_forces,read_in_geo
                          
  implicit none
  
  !--------------------
  !Declarations
  !--------------------  
  !MPI
  integer, parameter :: requiredThreading=MPI_THREAD_FUNNELED
  integer            :: providedThreading,rank,numprocs,ierr
  type(comms_type)   :: DLPOLY_comm
  type(mpifx_comm)   :: DFTB_globalMpiComm

  !Geometry
  type(configuration_type) :: config
  type(TGeometry)          :: geo
  
  !MD  
  integer                  :: istep,Nsteps
  type(MDstatus_type)      :: MDstatus 
  real(wp),   allocatable  :: forces(:,:)
  character(len=50)        :: fname
  character(len=2)         :: step_lab

  !--------------------
  !Main Routine
  !--------------------

  !Initialise MPI environment
  providedThreading = 0
  ierr = 0
  call mpi_init_thread(requiredThreading, providedThreading,ierr)
  call mpi_comm_rank(MPI_COMM_WORLD,rank,ierr)
  call mpi_comm_size(MPI_COMM_WORLD,numprocs,ierr)
  call fill_DLPOLY_comm(rank,numprocs, DLPOLY_comm)
  call fill_DFTB_mpifx_comm(DLPOLY_comm, DFTB_globalMpiComm)

  !Initialise geometry 
  call read_DLPOLY_vars(config)
  call fill_DFTB_geometry(config, geo)
  allocate(forces(3,geo%nAtom))

  !Initialise MD
  Nsteps=7
  MDstatus = MDstatus_type(DLPOLY_initial_MDstep,DLPOLY_initial_MDstep,Nsteps-1)  
  if(rank==0)then
     write(*,*) 'Call test running for MDsteps: [',&
          MDstatus%initial_step,',',MDstatus%final_step,']'
  endif
  
  !MD loop 
  do istep=MDstatus%initial_step,MDstatus%final_step
     MDstatus%current_step=istep

     write( step_lab, '(I2)' ) istep
     fname = '../inputs/set2/structure_'//trim(adjustl(step_lab))//'.gen'
     call read_in_geo(trim(adjustl(fname)),geo)
     
     if(rank==0) call print_geometry_data(geo,istep)
     call dftbplus(MDstatus,DFTB_globalMpiComm,geo,forces)
     if(rank==0) call output_forces(istep, forces, 'Ha/Bohr')  

     !Read in atomic positions for istep+1
     !write( step_lab, '(I2)' ) istep+1
     !fname = '../inputs/structure_'//trim(adjustl(step_lab))//'.gen'
     !if(istep /=  MDstatus%final_step) call read_in_geo(trim(adjustl(fname)),geo)
  enddo

 
end program call_lib
